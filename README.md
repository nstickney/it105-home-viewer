# IT105 Home Viewer

[![BSD 3-Clause Clear](https://img.shields.io/badge/license-BSD%203--Clause%20Clear-yellowgreen.svg)](LICENSE "BSD 3-Clause Clear License") [![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme "RichardLitt/standard-readme")

> A sample software project for teaching the iterative design process

IT105: Introduction to Computing and Information Technology

## Table of Contents

- [Background](#background)
- [Usage](#usage)
- [Contribute](#contribute)
- [License](#license)

## Background

This is a sample project. The `exercise.html` file uses the Google Maps API to view a given address.

## Usage

Open the file `exercise.html` in any web browser. You will need JavaScript enabled and a working internet connection with Google access.

## Contribute

> Contributors to this project are expected to adhere to our [Code of Conduct](CODE_OF_CONDUCT.md "Code of Conduct").

We welcome [issues](docs/issue_template.md "Issue template"), but we prefer [pull requests](dosc/pull_request_template.md "Pull request template")! See the [contribution guidelines](docs/contributing.md "Contributing") for more information.

## License

Copyright (c) 2018 tababbit.
All rights reserved.
